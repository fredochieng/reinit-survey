import {NavigationModelInterface} from '@gaxon/components';

export class NavigationModel implements NavigationModelInterface {
  public navigation: any[];

  constructor() {
    this.navigation = [
      {
        id: 'main',
        title: 'Main',
        translate: 'NAV.MAIN.TITLE',
        type: 'group',
        icon: '',
        children: [
          {
            id: 'sample-page',
            title: 'Dashboard',
            type: 'item',
            icon: 'dashboard2',
            url: '/dashboard'
          }
        ]
      }
    ];
  }
}
