import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListingLayoutComponent} from './listing-layout.component';

const routes: Routes = [
  {
    path: '',
    component: ListingLayoutComponent,
    children: [
      {
        path: 'ghghghghgh',
        loadChildren: () => import('@app/content/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('@app/content/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: '',
        loadChildren: () => import('@app/content/projects/projects.module').then(m => m.ProjectsModule)
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
